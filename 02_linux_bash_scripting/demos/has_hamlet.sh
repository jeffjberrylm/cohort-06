#!/bin/bash
FILENAME=$1

if grep -iq 'hamlet' $FILENAME; then
	echo $1 "contains hamlet"
else
	echo "No hamlet found"
fi
